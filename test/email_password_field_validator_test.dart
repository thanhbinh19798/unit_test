
import 'package:flutter_test/flutter_test.dart';
import 'package:unit_test/login_page.dart';

void main() {

  test('empty email returns error string', () {

    final result = EmailFieldValidator.validate('');
    expect(result, 'Email can\'t be empty');
  });

  test('non-empty email returns null', () {

    final result = EmailFieldValidator.validate('email');
    expect(result, null);
  });

  test('empty password returns error string', () {

    final result = PasswordFieldValidator.validate('');
    expect(result, 'Password can\'t be empty');
  });

  test('non-empty password returns null', () {

    final result = PasswordFieldValidator.validate('password');
    expect(result, null);
  });

  test('Login Success', () {
    final result = SuccessUserPassword.validate('', '');
    expect(result, 'Login Success');
  });

  test('Login Fail', () {
    final result = SuccessUserPassword.validate('abc', '123');
    expect(result, 'Login Fail');
  });
}